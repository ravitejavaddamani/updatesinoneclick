import { Component } from '@angular/core';
import { ManagerService } from '../services/manager.service';

@Component({
    selector: 'dashboard-page',
    templateUrl: './dashboard.html',

})
export class DashboardComponent {

    constructor(private managerService: ManagerService) {

    }
}