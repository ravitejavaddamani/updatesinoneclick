import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Router, NavigationStart, NavigationEnd, RouterModule, Routes } from '@angular/router';
import { StructureModule } from './structureModule';
import { AppComponent } from './app.component';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    StructureModule,
  ],

  declarations: [AppComponent],

  bootstrap: [AppComponent]
})
export class AppModule { }
