import { Injectable } from '@angular/core';
@Injectable()
export class Constants {


    public static compassPointsData = [
        { name: "North", value: 'N' },
        { name: "North by east", value: 'NbE' },
        { name: "North-northeast", value: 'NNE' },
        { name: "Northeast by north", value: 'NEbN' },
        { name: "Northeast", value: 'NEN' },
        { name: "Northeast by east	", value: 'NEbE' },
        { name: "East-northeast	", value: 'ENE' },
        { name: "East by north	", value: 'EbN' },
        { name: "East", value: 'E' },
        { name: "East by south", value: 'EbS' },
        { name: "East-southeast", value: 'ESE' },
        { name: "Southeast by east", value: 'SEbE' },
        { name: "Southeast", value: 'SE' },
        { name: "Southeast by south", value: 'SEbS' },
        { name: "South-southeast", value: 'SSE' },
        { name: "South by east	", value: 'SbE' },
        { name: "South", value: 'S' },
        { name: "South by west", value: 'SbW' },
        { name: "South-southwest", value: 'SSW' },
        { name: "Southwest by south", value: 'SWbS' },
        { name: "Southwest", value: 'SW' },
        { name: "Southwest by west", value: 'SWbW' },
        { name: "West-southwest", value: 'WSW' },
        { name: "West by south", value: 'WbS' },
        { name: "West", value: 'W' },
        { name: "West by north", value: 'WbN' },
        { name: "West-northwest", value: 'WNW' },
        { name: "Northwest by west", value: 'NWbW' },
        { name: "Northwest", value: 'NW' },
        { name: "Northwest by north", value: 'NWbN' },
        { name: "North-northwest", value: 'NNW' },
        { name: "North by west", value: 'NbW' },
        { name: "North East", value: 'NE' },
    ]

    public static getCompassNameByValue(value: string): string {
        for (let val of this.compassPointsData) {
            if (val.value == value) {
                return val.name;
            }
        }
    }

}

