"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var manager_service_1 = require("../services/manager.service");
var home_component_1 = require("../home/home.component");
var TrainBwStationsComponent = (function () {
    function TrainBwStationsComponent(managerService) {
        this.managerService = managerService;
        this.trainDetails = {};
        this.trains = [];
        this.sourceStationsList = {};
        this.destinationStationsList = {};
        this.todayDate = home_component_1.HomeComponent.convertTimeStampToDDMMYYYYFormat(new Date().getTime());
    }
    TrainBwStationsComponent.prototype.getTrainsBwStations = function () {
        var _this = this;
        var code1 = this.sourceCode;
        var code2 = this.destCode;
        this.errorMessage = '';
        var url = 'between/source/' + code1 + '/dest/' + code2 + '/date/' + this.todayDate + '/apikey/70bhga8dqu/';
        this.managerService.getData(url).subscribe(function (data) {
            if (data.response_code == '200') {
                alert(JSON.stringify(data));
                _this.trainDetails = data;
            }
            else {
                _this.errorMessage = 'Invalid Train station names. Please Try Again!';
            }
        }, function (error) {
        });
    };
    TrainBwStationsComponent.prototype.getSourceStationsList = function (sourceStation) {
        var _this = this;
        this.errorMessage = '';
        sourceStation = sourceStation.toUpperCase();
        var url = 'suggest-station/name/' + sourceStation + '/apikey/70bhga8dqu/';
        this.managerService.getData(url).subscribe(function (data) {
            if (data.response_code == '200') {
                _this.sourceStationsList = {};
                _this.sourceStationsList = data;
            }
            else {
                _this.errorMessage = 'Invalid Train Name. Please Try Again!';
            }
        }, function (error) {
        });
    };
    TrainBwStationsComponent.prototype.getSourceStationCode = function (code) {
        this.selectedSourceStationCode = '';
        this.selectedSourceStationCode = code;
    };
    TrainBwStationsComponent.prototype.getDestinationStationCode = function (code) {
        this.selecteDestinationStationCode = '';
        this.selecteDestinationStationCode = code;
    };
    TrainBwStationsComponent.prototype.getDestinationStationsList = function (destinationStation) {
        var _this = this;
        this.errorMessage = '';
        destinationStation = destinationStation.toUpperCase();
        var url = 'suggest-station/name/' + destinationStation + '/apikey/70bhga8dqu/';
        this.managerService.getData(url).subscribe(function (data) {
            if (data.response_code == '200') {
                _this.destinationStationsList = {};
                _this.destinationStationsList = data;
            }
            else {
                _this.errorMessage = 'Invalid Train Name. Please Try Again!';
            }
        }, function (error) {
        });
    };
    return TrainBwStationsComponent;
}());
TrainBwStationsComponent = __decorate([
    core_1.Component({
        selector: 'trainBwStations-page',
        templateUrl: './trainBwStations.html',
        styleUrls: ['./trainBwStations.scss']
    }),
    __metadata("design:paramtypes", [manager_service_1.ManagerService])
], TrainBwStationsComponent);
exports.TrainBwStationsComponent = TrainBwStationsComponent;
//# sourceMappingURL=trainBwStations.component.js.map