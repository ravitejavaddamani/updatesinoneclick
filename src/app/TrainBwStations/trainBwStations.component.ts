import { Component } from '@angular/core';
import { ManagerService } from '../services/manager.service';
import { FormsModule } from '@angular/forms';

import { HomeComponent } from '../home/home.component';

@Component({
    selector: 'trainBwStations-page',
    templateUrl: './trainBwStations.html',
    styleUrls: ['./trainBwStations.scss']

})
export class TrainBwStationsComponent {

    trainName: any;
    trainDetails: any = {};
    errorMessage: any;
    trains: any[] = [];
    todayDate: any;
    sourceCode: any;
    destCode: any;
    sourceStation: any;
    sourceStationsList: any = {};

    destinationStation: any;
    destinationStationsList: any = {};
    selectedSourceStationCode: any;
    selecteDestinationStationCode: any;


    constructor(private managerService: ManagerService) {
        this.todayDate = HomeComponent.convertTimeStampToDDMMYYYYFormat(new Date().getTime());
    }


    getTrainsBwStations() {
        
        var code1 = this.sourceCode;
        var code2 = this.destCode;
        this.errorMessage = '';
        var url = 'between/source/' + code1 + '/dest/' + code2 + '/date/' + this.todayDate + '/apikey/70bhga8dqu/';
        this.managerService.getData(url).subscribe(
            data => {
                if (data.response_code == '200') {
                    alert(JSON.stringify(data));
                    this.trainDetails = data;
                } else {
                    this.errorMessage = 'Invalid Train station names. Please Try Again!'
                }

            },
            error => {
            }
        )
    }

    getSourceStationsList(sourceStation: any) {
        this.errorMessage = '';
        sourceStation = sourceStation.toUpperCase();
        var url = 'suggest-station/name/' + sourceStation + '/apikey/70bhga8dqu/';
        this.managerService.getData(url).subscribe(
            data => {
                if (data.response_code == '200') {
                    this.sourceStationsList = {};
                    this.sourceStationsList = data;
                } else {
                    this.errorMessage = 'Invalid Train Name. Please Try Again!'
                }

            },
            error => {
            }
        )
    }

    getSourceStationCode(code: any) {
        this.selectedSourceStationCode = '';
        this.selectedSourceStationCode = code;
    }

    getDestinationStationCode(code: any) {
        this.selecteDestinationStationCode = '';
        this.selecteDestinationStationCode = code;
    }

    getDestinationStationsList(destinationStation: any) {
        this.errorMessage = '';
        destinationStation = destinationStation.toUpperCase();
        var url = 'suggest-station/name/' + destinationStation + '/apikey/70bhga8dqu/';
        this.managerService.getData(url).subscribe(
            data => {
                if (data.response_code == '200') {
                    this.destinationStationsList = {};
                    this.destinationStationsList = data;
                } else {
                    this.errorMessage = 'Invalid Train Name. Please Try Again!'
                }

            },
            error => {
            }
        )
    }

}