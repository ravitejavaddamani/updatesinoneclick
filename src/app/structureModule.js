"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var manager_service_1 = require("./services/manager.service");
var constants_1 = require("./constants");
var dashboard_component_1 = require("./dashboard/dashboard.component");
var login_component_1 = require("./login/login.component");
var home_component_1 = require("./home/home.component");
var menu_top_bar_component_1 = require("./navigation/menu-top-bar.component");
var liveStatus_component_1 = require("./liveStatus/liveStatus.component");
var trainBwStations_component_1 = require("./TrainBwStations/trainBwStations.component");
var trainsInformation_component_1 = require("./trainsInformation/trainsInformation.component");
var currentWhetherInfo_component_1 = require("./whetherInformation/currentWhetherInfo.component");
var pnrStatus_component_1 = require("./pnr_Status/pnrStatus.component");
var trainRoute_component_1 = require("./trainRoute/trainRoute.component");
var routes = [
    { path: '', redirectTo: '/login', pathMatch: 'full' },
    { path: 'login', component: login_component_1.LoginComponent },
    {
        path: 'home', component: home_component_1.HomeComponent,
        children: [
            { path: 'dashboard', component: dashboard_component_1.DashboardComponent },
            { path: 'liveStatus', component: liveStatus_component_1.LiveStatusComponent },
            { path: 'trainBwStations', component: trainBwStations_component_1.TrainBwStationsComponent },
            { path: 'trainsInfo', component: trainsInformation_component_1.TrainsInformationComponent },
            { path: 'CurrentWhetherInfo', component: currentWhetherInfo_component_1.CurrentWhetherInfoComponent },
        ],
    },
];
var StructureModule = (function () {
    function StructureModule() {
    }
    return StructureModule;
}());
StructureModule = __decorate([
    core_1.NgModule({
        imports: [
            common_1.CommonModule,
            forms_1.FormsModule,
            router_1.RouterModule.forChild(routes),
            router_1.RouterModule.forRoot(routes),
        ],
        exports: [router_1.RouterModule],
        declarations: [
            dashboard_component_1.DashboardComponent,
            login_component_1.LoginComponent,
            home_component_1.HomeComponent,
            menu_top_bar_component_1.MenuTopBarComponent,
            liveStatus_component_1.LiveStatusComponent,
            trainBwStations_component_1.TrainBwStationsComponent,
            trainsInformation_component_1.TrainsInformationComponent,
            currentWhetherInfo_component_1.CurrentWhetherInfoComponent,
            pnrStatus_component_1.PNRStatusComponent,
            trainRoute_component_1.TrainTouteComponent
        ],
        providers: [
            manager_service_1.ManagerService,
            constants_1.Constants
        ],
    })
], StructureModule);
exports.StructureModule = StructureModule;
//# sourceMappingURL=structureModule.js.map