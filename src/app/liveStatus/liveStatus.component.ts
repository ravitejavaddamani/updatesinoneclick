import { Component } from '@angular/core';
import { ManagerService } from '../services/manager.service';
import { FormsModule } from '@angular/forms';

import { HomeComponent } from '../home/home.component';
@Component({
    selector: 'liveStatus-page',
    templateUrl: './liveStatus.html',
    styleUrls: ['./liveStatus.scss']
})
export class LiveStatusComponent {

    trainName: any;
    trainDetails: any = {};
    errorMessage: any;
    trains: any[] = [];
    todayDate: any;

    constructor(private managerService: ManagerService) {
        this.todayDate = HomeComponent.convertTimeStampToDDMMYYYYFormat(new Date().getTime());
    }

    getTrainLiveStatus(trainNumber: any) {

        this.errorMessage = '';
        var url = 'live/train/' + trainNumber + '/date/' + this.todayDate + '/apikey/70bhga8dqu/';
        this.managerService.getData(url).subscribe(
            data => {
                if (data.response_code == '200') {
                    this.trainDetails = data;
                } else {
                    this.errorMessage = 'Invalid Train Number. Please Try Again!'
                }

            },
            error => {
            }
        )
    }

    getTrainNumber(name: any) {
        this.errorMessage = '';
        var url = 'name-number/train/' + name + '/apikey/70bhga8dqu/';
        this.managerService.getData(url).subscribe(
            data => {
                if (data.response_code == '200') {
                    this.trains.push(data);
                } else {
                    this.errorMessage = 'Invalid Train Number. Please Try Again!'
                }

            },
            error => {
            }
        )
    }
   
    

}