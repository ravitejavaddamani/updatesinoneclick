"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var manager_service_1 = require("../services/manager.service");
var TrainTouteComponent = (function () {
    function TrainTouteComponent(managerService) {
        this.managerService = managerService;
        this.trainDetails = {};
        this.trains = [];
        this.trainRunsOn = '';
    }
    TrainTouteComponent.prototype.getTrainRoute = function (trainNumber) {
        var _this = this;
        this.errorMessage = '';
        var url = 'route/train/' + trainNumber + '/apikey/70bhga8dqu/';
        this.managerService.getData(url).subscribe(function (data) {
            if (data.response_code == '200') {
                _this.trainDetails = data;
                for (var _i = 0, _a = _this.trainDetails.train.days; _i < _a.length; _i++) {
                    var train = _a[_i];
                    if (train.runs == 'Y') {
                        _this.trainRunsOn = _this.trainRunsOn + train.code + ' , ';
                    }
                }
            }
            else {
                _this.errorMessage = 'Invalid Train Number. Please Try Again!';
            }
        }, function (error) {
        });
    };
    TrainTouteComponent.prototype.getTrainNumber = function (name) {
        var _this = this;
        this.errorMessage = '';
        var url = 'name-number/train/' + name + '/apikey/70bhga8dqu/';
        this.managerService.getData(url).subscribe(function (data) {
            if (data.response_code == '200') {
                _this.trains.push(data);
            }
            else {
                _this.errorMessage = 'Invalid Train Number. Please Try Again!';
            }
        }, function (error) {
        });
    };
    return TrainTouteComponent;
}());
TrainTouteComponent = __decorate([
    core_1.Component({
        selector: 'trainRoute-page',
        templateUrl: './trainRoute.html',
        styleUrls: ['./trainRoute.scss']
    }),
    __metadata("design:paramtypes", [manager_service_1.ManagerService])
], TrainTouteComponent);
exports.TrainTouteComponent = TrainTouteComponent;
//# sourceMappingURL=trainRoute.component.js.map