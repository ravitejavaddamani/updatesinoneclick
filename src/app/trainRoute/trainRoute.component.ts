import { Component } from '@angular/core';
import { ManagerService } from '../services/manager.service';
import { FormsModule } from '@angular/forms';

import { HomeComponent } from '../home/home.component';
@Component({
    selector: 'trainRoute-page',
    templateUrl: './trainRoute.html',
    styleUrls: ['./trainRoute.scss']
})
export class TrainTouteComponent {

    trainName: any;
    trainDetails: any = {};
    errorMessage: any;
    trains: any[] = [];
    trainRunsOn: any = '';

    constructor(private managerService: ManagerService) {
    }

    getTrainRoute(trainNumber: any) {

        this.errorMessage = '';
        var url = 'route/train/' + trainNumber + '/apikey/70bhga8dqu/';
        this.managerService.getData(url).subscribe(
            data => {
                if (data.response_code == '200') {
                    this.trainDetails = data;
                    for (let train of this.trainDetails.train.days) {
                        if (train.runs == 'Y') {
                            this.trainRunsOn = this.trainRunsOn + train.code + ' , '
                        }
                    }
                } else {
                    this.errorMessage = 'Invalid Train Number. Please Try Again!'
                }

            },
            error => {
            }
        )
    }

    getTrainNumber(name: any) {
        this.errorMessage = '';
        var url = 'name-number/train/' + name + '/apikey/70bhga8dqu/';
        this.managerService.getData(url).subscribe(
            data => {
                if (data.response_code == '200') {
                    this.trains.push(data);
                } else {
                    this.errorMessage = 'Invalid Train Number. Please Try Again!'
                }

            },
            error => {
            }
        )
    }



}