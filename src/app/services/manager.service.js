"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var router_1 = require("@angular/router");
require("rxjs/add/operator/map");
var ManagerService = (function () {
    function ManagerService(http, router) {
        this.http = http;
        this.router = router;
        this.URL = 'https://api.railwayapi.com/v2/';
        this.WHETHER_URL = 'http://api.apixu.com/v1/history.json';
    }
    ManagerService.prototype.getJsonHeaders = function () {
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        var options = new http_1.RequestOptions({ headers: headers });
        return options;
    };
    ;
    ManagerService.prototype.getData = function (pathToHit) {
        return this.http.get((this.URL + pathToHit), this.getJsonHeaders()).map(function (response) {
            var data = response.json();
            return data;
        });
    };
    ManagerService.prototype.getWhetherData = function (cityName, apiKey, date) {
        return this.http.get((this.WHETHER_URL + '?key=' + apiKey + '&q=' + cityName + '&dt=' + date), this.getJsonHeaders()).map(function (response) {
            var data = response.json();
            return data;
        });
    };
    ManagerService.prototype.getWhetherDataDaily = function (cityName) {
        return this.http.get(('http://api.apixu.com/v1/forecast.json?key=c4bfd72393394abb91674638182901&q=' + cityName + '&days=7'), this.getJsonHeaders()).map(function (response) {
            var data = response.json();
            return data;
        });
    };
    return ManagerService;
}());
ManagerService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http, router_1.Router])
], ManagerService);
exports.ManagerService = ManagerService;
//# sourceMappingURL=manager.service.js.map