import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestMethod, RequestOptions, ResponseContentType } from '@angular/http';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';

@Injectable()
export class ManagerService {

    public URL = 'https://api.railwayapi.com/v2/';

    public WHETHER_URL = 'http://api.apixu.com/v1/history.json';

    constructor(public http: Http, private router: Router) {
    }

    public getJsonHeaders() {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({ headers: headers });
        return options;
    };


    public getData(pathToHit: any) {
        return this.http.get((this.URL + pathToHit), this.getJsonHeaders()).map((response: Response) => {
            var data = response.json();
            return data;
        });

    }

    public getWhetherData(cityName: any, apiKey: any, date: any) {
        return this.http.get((this.WHETHER_URL + '?key=' + apiKey + '&q=' + cityName + '&dt=' + date), this.getJsonHeaders()).map((response: Response) => {
            var data = response.json();
            return data;
        });

    }

    public getWhetherDataDaily(cityName: any) {
        return this.http.get(('http://api.apixu.com/v1/forecast.json?key=c4bfd72393394abb91674638182901&q=' + cityName + '&days=7'), this.getJsonHeaders()).map((response: Response) => {
            var data = response.json();
            return data;
        });

    }


}
