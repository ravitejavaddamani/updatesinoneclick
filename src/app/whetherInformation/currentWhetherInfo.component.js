"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var manager_service_1 = require("../services/manager.service");
var constants_1 = require("../../app/constants");
var home_component_1 = require("../home/home.component");
var CurrentWhetherInfoComponent = (function () {
    function CurrentWhetherInfoComponent(managerService) {
        this.managerService = managerService;
        this.apikey = 'c4bfd72393394abb91674638182901';
        this.totalWhetherDetails = {};
        this.locationData = {};
        this.forcastDayData = {};
        this.astroData = {};
        this.forecastHourlyData = [];
        this.whetherTemperatureChartYAxisValues = [];
        this.whetherWindChartYAxisValues = [];
        this.whetherPressureChartYAxisValues = [];
        this.dailyForecastData = [];
        this.dailyForecastXAxisValues = [];
        this.dailyForecastYAxisValues = [];
        this.todayDate = home_component_1.HomeComponent.convertTimeStampToYYYYMMDDFormat(new Date().getTime());
        this.todayDateWithName = home_component_1.HomeComponent.convertTimeStampToDateWithMonth(new Date().getTime());
        this.dateWithHours = this.todayDate + ' ' + new Date().getHours() + ':00';
    }
    CurrentWhetherInfoComponent.prototype.ngOnInit = function () {
    };
    CurrentWhetherInfoComponent.prototype.getWhetherInfo = function (city) {
        var _this = this;
        this.totalWhetherDetails = {};
        this.locationData = {};
        this.forcastDayData = {};
        this.astroData = {};
        this.forecastHourlyData = [];
        this.whetherTemperatureChartYAxisValues = [];
        this.whetherWindChartYAxisValues = [];
        this.whetherPressureChartYAxisValues = [];
        this.dailyForecastData = [];
        this.dailyForecastXAxisValues = [];
        this.dailyForecastYAxisValues = [];
        this.managerService.getWhetherData(city, this.apikey, this.todayDate).subscribe(function (data) {
            _this.totalWhetherDetails = data;
            _this.locationData = _this.totalWhetherDetails.location;
            _this.astroData = _this.totalWhetherDetails.forecast.forecastday[0].astro;
            _this.forecastHourlyData = _this.totalWhetherDetails.forecast.forecastday[0].hour;
            for (var _i = 0, _a = _this.forecastHourlyData; _i < _a.length; _i++) {
                var data_1 = _a[_i];
                if (data_1.time == _this.dateWithHours) {
                    _this.forcastDayData = data_1;
                }
            }
            var windDirrection = constants_1.Constants.getCompassNameByValue(_this.forcastDayData.wind_dir);
            if (windDirrection != undefined) {
                _this.forcastDayData.wind_dir = windDirrection;
            }
            _this.drawWhetherTemperatureChart();
            _this.drawWhetherWindChart();
            _this.drawWhetherPressureChart();
            _this.getLastSevenDaysWhetherInfo();
        }, function (error) {
        });
    };
    CurrentWhetherInfoComponent.prototype.getLastSevenDaysWhetherInfo = function () {
        var _this = this;
        this.managerService.getWhetherDataDaily(this.cityName).subscribe(function (data) {
            _this.dailyForecastData = data.forecast.forecastday;
            _this.drawDailyForecastChart();
        }, function (error) {
        });
    };
    CurrentWhetherInfoComponent.prototype.getIconPath = function (iconPath) {
        return 'https:' + iconPath;
    };
    CurrentWhetherInfoComponent.prototype.drawWhetherTemperatureChart = function () {
        this.whetherTemperatureChartYAxisValues.push('Temperature In Centigrade');
        for (var _i = 0, _a = this.forecastHourlyData; _i < _a.length; _i++) {
            var data = _a[_i];
            this.whetherTemperatureChartYAxisValues.push(data.temp_c);
        }
        var chart = c3.generate({
            bindto: ".temperature_line_chart",
            data: {
                columns: [
                    this.whetherTemperatureChartYAxisValues,
                ]
            }
        });
    };
    CurrentWhetherInfoComponent.prototype.drawWhetherWindChart = function () {
        this.whetherWindChartYAxisValues.push('Wind Speed In Kilometer Per Hour');
        for (var _i = 0, _a = this.forecastHourlyData; _i < _a.length; _i++) {
            var data = _a[_i];
            this.whetherWindChartYAxisValues.push(data.wind_kph);
        }
        var chart = c3.generate({
            bindto: ".wind_line_chart",
            data: {
                columns: [
                    this.whetherWindChartYAxisValues,
                ]
            }
        });
    };
    CurrentWhetherInfoComponent.prototype.drawWhetherPressureChart = function () {
        this.whetherPressureChartYAxisValues.push('Pressure In Inches');
        for (var _i = 0, _a = this.forecastHourlyData; _i < _a.length; _i++) {
            var data = _a[_i];
            this.whetherPressureChartYAxisValues.push(data.pressure_in);
        }
        var chart = c3.generate({
            bindto: ".pressure_line_chart",
            data: {
                columns: [
                    this.whetherPressureChartYAxisValues,
                ]
            }
        });
    };
    CurrentWhetherInfoComponent.prototype.drawDailyForecastChart = function () {
        this.dailyForecastXAxisValues.push('x');
        this.dailyForecastYAxisValues.push('Temperature');
        for (var _i = 0, _a = this.dailyForecastData; _i < _a.length; _i++) {
            var data = _a[_i];
            this.dailyForecastXAxisValues.push(data.date);
            this.dailyForecastYAxisValues.push(data.day.mintemp_c);
        }
        var chart = c3.generate({
            bindto: ".daily_line_chart",
            data: {
                x: 'x',
                columns: [
                    this.dailyForecastXAxisValues,
                    this.dailyForecastYAxisValues,
                ],
                types: {
                    Temperature: 'area',
                },
            },
            point: {
                show: false
            },
            axis: {
                x: {
                    type: 'timeseries',
                    tick: {
                        format: '%Y-%m-%d'
                    }
                }
            }
        });
    };
    CurrentWhetherInfoComponent.prototype.splitDateAndTime = function (date) {
        var arr = date.split(' ');
        return arr[1];
    };
    return CurrentWhetherInfoComponent;
}());
CurrentWhetherInfoComponent = __decorate([
    core_1.Component({
        selector: 'currentWhetherInfo-page',
        templateUrl: './currentWhetherInfo.html',
        styleUrls: ['./currentWhetherInfo.scss']
    }),
    __metadata("design:paramtypes", [manager_service_1.ManagerService])
], CurrentWhetherInfoComponent);
exports.CurrentWhetherInfoComponent = CurrentWhetherInfoComponent;
//# sourceMappingURL=currentWhetherInfo.component.js.map