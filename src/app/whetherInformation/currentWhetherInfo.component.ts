import { Component } from '@angular/core';
import { ManagerService } from '../services/manager.service';
import { Constants } from '../../app/constants';

import { HomeComponent } from '../home/home.component';

declare var c3: any;

@Component({
    selector: 'currentWhetherInfo-page',
    templateUrl: './currentWhetherInfo.html',
    styleUrls: ['./currentWhetherInfo.scss']

})
export class CurrentWhetherInfoComponent {

    cityName: any;
    apikey = 'c4bfd72393394abb91674638182901';
    totalWhetherDetails: any = {};

    locationData: any = {};
    forcastDayData: any = {};
    astroData: any = {};

    forecastHourlyData: any[] = [];
    iconPath: any;

    todayDate: any;
    todayDateWithName: any;
    dateWithHours: any;

    whetherTemperatureChartYAxisValues: any[] = [];
    whetherWindChartYAxisValues: any[] = [];
    whetherPressureChartYAxisValues: any[] = [];

    dailyForecastData: any[] = [];
    dailyForecastXAxisValues: any[] = [];
    dailyForecastYAxisValues: any[] = [];

    constructor(private managerService: ManagerService) {
        this.todayDate = HomeComponent.convertTimeStampToYYYYMMDDFormat(new Date().getTime());
        this.todayDateWithName = HomeComponent.convertTimeStampToDateWithMonth(new Date().getTime());
        this.dateWithHours = this.todayDate + ' ' + new Date().getHours() + ':00';
    }

    ngOnInit() {
    }


    getWhetherInfo(city: any) {

        this.totalWhetherDetails = {};
        this.locationData = {};
        this.forcastDayData = {};
        this.astroData = {};
        this.forecastHourlyData = [];
        this.whetherTemperatureChartYAxisValues = [];
        this.whetherWindChartYAxisValues = [];
        this.whetherPressureChartYAxisValues = [];
        this.dailyForecastData = [];
        this.dailyForecastXAxisValues = [];
        this.dailyForecastYAxisValues = [];

        this.managerService.getWhetherData(city, this.apikey, this.todayDate).subscribe(
            data => {
                this.totalWhetherDetails = data;
                this.locationData = this.totalWhetherDetails.location;
                this.astroData = this.totalWhetherDetails.forecast.forecastday[0].astro;
                this.forecastHourlyData = this.totalWhetherDetails.forecast.forecastday[0].hour;

                for (let data of this.forecastHourlyData) {
                    if (data.time == this.dateWithHours) {
                        this.forcastDayData = data;
                    }
                }
                var windDirrection = Constants.getCompassNameByValue(this.forcastDayData.wind_dir);
                if (windDirrection != undefined) {
                    this.forcastDayData.wind_dir = windDirrection;
                }
                this.drawWhetherTemperatureChart();
                this.drawWhetherWindChart();
                this.drawWhetherPressureChart();
                this.getLastSevenDaysWhetherInfo();

            },
            error => {
            }
        )
    }

    getLastSevenDaysWhetherInfo() {
        this.managerService.getWhetherDataDaily(this.cityName).subscribe(
            data => {
                this.dailyForecastData = data.forecast.forecastday;
                this.drawDailyForecastChart();

            },
            error => {
            }
        )
    }


    getIconPath(iconPath: any) {
        return 'https:' + iconPath;
    }

    drawWhetherTemperatureChart() {
        this.whetherTemperatureChartYAxisValues.push('Temperature In Centigrade');
        for (let data of this.forecastHourlyData) {
            this.whetherTemperatureChartYAxisValues.push(data.temp_c);
        }
        var chart = c3.generate({
            bindto: ".temperature_line_chart",
            data: {
                columns: [
                    this.whetherTemperatureChartYAxisValues,
                ]
            }
        });
    }

    drawWhetherWindChart() {
        this.whetherWindChartYAxisValues.push('Wind Speed In Kilometer Per Hour');
        for (let data of this.forecastHourlyData) {
            this.whetherWindChartYAxisValues.push(data.wind_kph);
        }
        var chart = c3.generate({
            bindto: ".wind_line_chart",
            data: {
                columns: [
                    this.whetherWindChartYAxisValues,
                ]
            }
        });
    }

    drawWhetherPressureChart() {
        this.whetherPressureChartYAxisValues.push('Pressure In Inches');
        for (let data of this.forecastHourlyData) {
            this.whetherPressureChartYAxisValues.push(data.pressure_in);
        }
        var chart = c3.generate({
            bindto: ".pressure_line_chart",
            data: {
                columns: [
                    this.whetherPressureChartYAxisValues,
                ]
            }
        });
    }

    drawDailyForecastChart() {
        this.dailyForecastXAxisValues.push('x');
        this.dailyForecastYAxisValues.push('Temperature');

        for (let data of this.dailyForecastData) {
            this.dailyForecastXAxisValues.push(data.date);
            this.dailyForecastYAxisValues.push(data.day.mintemp_c);
        }

        var chart = c3.generate({
            bindto: ".daily_line_chart",
            data: {
                x: 'x',
                columns: [
                    this.dailyForecastXAxisValues,
                    this.dailyForecastYAxisValues,
                ],
                types: {
                    Temperature: 'area',
                },
            },
            point: {
                show: false
            },
            axis: {
                x: {
                    type: 'timeseries',
                    tick: {
                        format: '%Y-%m-%d'
                    }
                }
            }
        });
    }

    splitDateAndTime(date: any) {
        var arr = date.split(' ');
        return arr[1];
    }
}