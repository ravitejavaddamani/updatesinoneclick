import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'login-page',
    templateUrl: './login.html',
    styleUrls: ['./login.scss']
})
export class LoginComponent {

    constructor(private router: Router) {

    }

    doLogin() {
        this.router.navigate(['home/dashboard']);
    }
}