import { NgModule } from '@angular/core';
import { BrowserModule, } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ManagerService } from './services/manager.service';
import { Constants } from './constants';

import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { MenuTopBarComponent } from './navigation/menu-top-bar.component';
import { LiveStatusComponent } from './liveStatus/liveStatus.component';
import { TrainBwStationsComponent } from './TrainBwStations/trainBwStations.component';
import { TrainsInformationComponent } from './trainsInformation/trainsInformation.component';
import { CurrentWhetherInfoComponent } from './whetherInformation/currentWhetherInfo.component';
import { PNRStatusComponent } from './pnr_Status/pnrStatus.component';
import { TrainTouteComponent } from './trainRoute/trainRoute.component';

const routes: Routes = [
    { path: '', redirectTo: '/login', pathMatch: 'full' },


    { path: 'login', component: LoginComponent },
    {
        path: 'home', component: HomeComponent,
        children: [
            { path: 'dashboard', component: DashboardComponent },
            { path: 'liveStatus', component: LiveStatusComponent },
            { path: 'trainBwStations', component: TrainBwStationsComponent },
            { path: 'trainsInfo', component: TrainsInformationComponent },
            { path: 'CurrentWhetherInfo', component: CurrentWhetherInfoComponent },
        ],
    },
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild(routes),
        RouterModule.forRoot(routes),
    ],
    exports: [RouterModule],
    declarations: [
        DashboardComponent,
        LoginComponent,
        HomeComponent,
        MenuTopBarComponent,
        LiveStatusComponent,
        TrainBwStationsComponent,
        TrainsInformationComponent,
        CurrentWhetherInfoComponent,
        PNRStatusComponent,
        TrainTouteComponent
    ],
    providers: [
        ManagerService,
        Constants
    ],
})
export class StructureModule { }