"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var Constants = (function () {
    function Constants() {
    }
    Constants.getCompassNameByValue = function (value) {
        for (var _i = 0, _a = this.compassPointsData; _i < _a.length; _i++) {
            var val = _a[_i];
            if (val.value == value) {
                return val.name;
            }
        }
    };
    return Constants;
}());
Constants.compassPointsData = [
    { name: "North", value: 'N' },
    { name: "North by east", value: 'NbE' },
    { name: "North-northeast", value: 'NNE' },
    { name: "Northeast by north", value: 'NEbN' },
    { name: "Northeast", value: 'NEN' },
    { name: "Northeast by east	", value: 'NEbE' },
    { name: "East-northeast	", value: 'ENE' },
    { name: "East by north	", value: 'EbN' },
    { name: "East", value: 'E' },
    { name: "East by south", value: 'EbS' },
    { name: "East-southeast", value: 'ESE' },
    { name: "Southeast by east", value: 'SEbE' },
    { name: "Southeast", value: 'SE' },
    { name: "Southeast by south", value: 'SEbS' },
    { name: "South-southeast", value: 'SSE' },
    { name: "South by east	", value: 'SbE' },
    { name: "South", value: 'S' },
    { name: "South by west", value: 'SbW' },
    { name: "South-southwest", value: 'SSW' },
    { name: "Southwest by south", value: 'SWbS' },
    { name: "Southwest", value: 'SW' },
    { name: "Southwest by west", value: 'SWbW' },
    { name: "West-southwest", value: 'WSW' },
    { name: "West by south", value: 'WbS' },
    { name: "West", value: 'W' },
    { name: "West by north", value: 'WbN' },
    { name: "West-northwest", value: 'WNW' },
    { name: "Northwest by west", value: 'NWbW' },
    { name: "Northwest", value: 'NW' },
    { name: "Northwest by north", value: 'NWbN' },
    { name: "North-northwest", value: 'NNW' },
    { name: "North by west", value: 'NbW' },
    { name: "North East", value: 'NE' },
];
Constants = __decorate([
    core_1.Injectable()
], Constants);
exports.Constants = Constants;
//# sourceMappingURL=constants.js.map