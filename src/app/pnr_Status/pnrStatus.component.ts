import { Component } from '@angular/core';
import { ManagerService } from '../services/manager.service';
import { FormsModule } from '@angular/forms';

import { HomeComponent } from '../home/home.component';

@Component({
    selector: 'pnrStatus-page',
    templateUrl: './pnrStatus.html',
    //styleUrls: ['./trainBwStations.scss']

})
export class PNRStatusComponent {

    pnrNumber: any;
    trainDetails: any = {};
    errorMessage = '';


    constructor(private managerService: ManagerService) {
    }


    getPnrStatus() {

        var url = 'pnr-status/pnr/' + this.pnrNumber + '/apikey/70bhga8dqu/';
        this.managerService.getData(url).subscribe(
            data => {
                if (data.response_code == '200') {
                    alert(JSON.stringify(data));
                    this.trainDetails = data;
                } else {
                    this.errorMessage = 'Invalid Train station names. Please Try Again!'
                }

            },
            error => {
            }
        )
    }



}