"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var manager_service_1 = require("../services/manager.service");
var PNRStatusComponent = (function () {
    function PNRStatusComponent(managerService) {
        this.managerService = managerService;
        this.trainDetails = {};
        this.errorMessage = '';
    }
    PNRStatusComponent.prototype.getPnrStatus = function () {
        var _this = this;
        var url = 'pnr-status/pnr/' + this.pnrNumber + '/apikey/70bhga8dqu/';
        this.managerService.getData(url).subscribe(function (data) {
            if (data.response_code == '200') {
                alert(JSON.stringify(data));
                _this.trainDetails = data;
            }
            else {
                _this.errorMessage = 'Invalid Train station names. Please Try Again!';
            }
        }, function (error) {
        });
    };
    return PNRStatusComponent;
}());
PNRStatusComponent = __decorate([
    core_1.Component({
        selector: 'pnrStatus-page',
        templateUrl: './pnrStatus.html',
    }),
    __metadata("design:paramtypes", [manager_service_1.ManagerService])
], PNRStatusComponent);
exports.PNRStatusComponent = PNRStatusComponent;
//# sourceMappingURL=pnrStatus.component.js.map