import { Component } from '@angular/core';
import { ManagerService } from '../services/manager.service';

import { LiveStatusComponent } from '../liveStatus/liveStatus.component';
import { TrainBwStationsComponent } from '../TrainBwStations/trainBwStations.component';
import { PNRStatusComponent } from '../pnr_Status/pnrStatus.component'
import { TrainTouteComponent } from '../trainRoute/trainRoute.component';

@Component({
    selector: 'trainsInfo-page',
    templateUrl: './trainsInformation.html',

})
export class TrainsInformationComponent {

    constructor(private managerService: ManagerService) {

    }
}