import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'top-bar',
    templateUrl: './menu-top-bar.html',
})
export class MenuTopBarComponent {

    constructor(private router: Router) {

    }

    goToTrainsInformation() {
        this.router.navigate(['home/trainsInfo']);
    }

    gotToWhetherInformation() {
        this.router.navigate(['home/CurrentWhetherInfo']);
    }

    doLogout() {
        this.router.navigate(['login']);
    }

}