"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var HomeComponent = (function () {
    function HomeComponent() {
    }
    HomeComponent.convertTimeStampToDDMMYYYYFormat = function (timeStamp) {
        var prorityDate = new Date(timeStamp);
        return prorityDate.getDate() + '-' + (prorityDate.getMonth() + 1) + '-' + prorityDate.getFullYear();
    };
    HomeComponent.convertTimeStampToYYYYMMDDFormat = function (timeStamp) {
        var prorityDate = new Date(timeStamp);
        if (prorityDate.getMonth() + 1 < 10) {
            return prorityDate.getFullYear() + '-0' + (prorityDate.getMonth() + 1) + '-' + prorityDate.getDate();
        }
        else {
            return prorityDate.getFullYear() + '-' + (prorityDate.getMonth() + 1) + '-' + prorityDate.getDate();
        }
    };
    HomeComponent.convertTimeStampToDateWithMonth = function (timeStamp) {
        var prorityDate = new Date(timeStamp);
        var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
        ];
        return monthNames[prorityDate.getMonth()] + ' ' + prorityDate.getDate() + ', '
            + prorityDate.getFullYear();
    };
    HomeComponent.openSpinnerPopUp = function () {
        $("#spinner").modal('show');
    };
    HomeComponent.closeSpinnerPopUp = function () {
        $("#spinner").modal('hide');
    };
    return HomeComponent;
}());
HomeComponent = __decorate([
    core_1.Component({
        selector: 'home-page',
        templateUrl: './home.html',
        styleUrls: ['./home.scss']
    })
], HomeComponent);
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=home.component.js.map