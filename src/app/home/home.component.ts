import { Component } from '@angular/core';
declare var $: any;

@Component({
    selector: 'home-page',
    templateUrl: './home.html',
    styleUrls: ['./home.scss']
})
export class HomeComponent {

    public static convertTimeStampToDDMMYYYYFormat(timeStamp: any) {
        var prorityDate = new Date(timeStamp);
        return prorityDate.getDate() + '-' + (prorityDate.getMonth() + 1) + '-' + prorityDate.getFullYear();
    }

    public static convertTimeStampToYYYYMMDDFormat(timeStamp: any) {
        var prorityDate = new Date(timeStamp);
        if (prorityDate.getMonth() + 1 < 10) {
            return prorityDate.getFullYear() + '-0' + (prorityDate.getMonth() + 1) + '-' + prorityDate.getDate();
        } else {
            return prorityDate.getFullYear() + '-' + (prorityDate.getMonth() + 1) + '-' + prorityDate.getDate();
        }
    }

    public static convertTimeStampToDateWithMonth(timeStamp: any) {
        var prorityDate = new Date(timeStamp);
        var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
        ];
        return monthNames[prorityDate.getMonth()] + ' ' + prorityDate.getDate() + ', '
            + prorityDate.getFullYear();
    }

    public static openSpinnerPopUp() {
        $("#spinner").modal('show');
    }

    public static closeSpinnerPopUp() {
        $("#spinner").modal('hide');
    }
}